import { dataShoeShop } from "../../dataShoeShop";
import { ADD_TO_CART, XOA_SAN_PHAM } from "../constants/constant";

let initialState = {
  productList: dataShoeShop,
  gioHang: [],
};

export const shoeShopReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case ADD_TO_CART: {
      console.log("yes", payload);

      let cloneGioHang = [...state.gioHang];

      let index = cloneGioHang.findIndex((item) => {
        return item.id == payload.id;
      });
      if (index == -1) {
        let newSanPham = { ...payload, soLuong: 1 };
        cloneGioHang.push(newSanPham);
      } else {
        cloneGioHang[index].soLuong++;
      }
      // cloneGioHang.push(sanPham);

      state.gioHang = cloneGioHang;

      let dataJson = JSON.stringify(state);
      state = JSON.parse(dataJson);

      return { ...state };
    }

    case XOA_SAN_PHAM: {
      console.log("yes", payload);
      let cloneGioHang = [...state.gioHang];
      let index = cloneGioHang.findIndex((item) => {
        return (item.id = payload);
      });

      if (index !== -1) {
        cloneGioHang.splice(index, 1);
        return { ...state };
      }
    }

    default:
      return state;
  }
};
