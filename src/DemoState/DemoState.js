import React, { Component } from "react";

export default class DemoState extends Component {
  state = {
    isLogin: true,
    username: "alice",
  };
  render() {
    let handleLogin = () => {
      // this.state.isLogin = true;
      this.setState({ isLogin: true });
      console.log("isLogin", this.state.isLogin);
    };
    let handleLogout = () => {
      // this.state.isLogin = false;
      this.setState({ isLogin: false });
      console.log("isLogin", this.state.isLogin);
    };
    let renderContent = () => {
      if (this.state.isLogin) {
        return (
          <div>
            <h3 className="text-success">Hello Alice</h3>
            <button onClick={handleLogout} className="btn btn-success">
              Logout
            </button>
          </div>
        );
      } else {
        return (
          <div>
            <h3 className="text-warning">Please Login</h3>
            <button onClick={handleLogin} className="btn btn-warning">
              Login
            </button>
          </div>
        );
      }
    };
    return <div className="container py-5">{renderContent()}</div>;
  }
}
