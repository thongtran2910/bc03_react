import React, { Component } from "react";

export default class ConditionRendering extends Component {
  render() {
    let isLogin = true;

    let handleLogin = () => {
      isLogin = true;
      console.log("isLogin", isLogin);
    };
    let handleLogout = () => {
      isLogin = false;
      console.log("isLogin", isLogin);
    };
    let renderContent = () => {
      if (isLogin) {
        return (
          <div>
            <h3 className="text-success">Hello Alice</h3>
            <button onClick={handleLogout} className="btn btn-success">
              Logout
            </button>
          </div>
        );
      } else {
        return (
          <div>
            <h3 className="text-warning">Please Login</h3>
            <button onClick={handleLogin} className="btn btn-warning">
              Login
            </button>
          </div>
        );
      }
    };
    return <div className="container py-5">{renderContent()}</div>;
  }
}
