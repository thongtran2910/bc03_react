import React, { Component } from "react";

export default class BaiTapChonXe extends Component {
  state = {
    urlImg: "./img/car/red-car.jpg",
    imgArr: [
      "./img/car/red-car.jpg",
      "./img/car/black-car.jpg",
      "./img/car/silver-car.jpg",
    ],
  };
  handleChangeCar = (url) => {
    this.setState({ urlImg: url });
  };
  render() {
    return (
      <div className="container py-5">
        <div className="row">
          <div className="col-6">
            <img className="w-100" src={this.state.urlImg} alt="" />
          </div>
          <div>
            <button
              onClick={() => {
                this.handleChangeCar("./img/car/red-car.jpg");
              }}
              className="btn btn-danger"
            >
              Red
            </button>
            <button
              onClick={() => {
                this.handleChangeCar("./img/car/black-car.jpg");
              }}
              className="btn btn-dark mx-5"
            >
              Black
            </button>
            <button
              onClick={() => {
                this.handleChangeCar("./img/car/silver-car.jpg");
              }}
              className="btn btn-secondary"
            >
              Silver
            </button>
          </div>
        </div>
        {this.state.imgArr.map((url) => {
          return <img style={{ width: "150px", margin: "30px" }} src={url} />;
        })}
      </div>
    );
  }
}
