import logo from "./logo.svg";
import "./App.css";
import DemoClass from "./DemoComponent/DemoClass";
import DemoFunction from "./DemoComponent/DemoFunction";
import BaiTapLayoutBS from "./BaiTapLayoutBS/BaiTapLayoutBS";
import DataBind from "./DataBind/DataBind";
import DemoState from "./DemoState/DemoState";
import BaiTapChonXe from "./BaiTapChonXe/BaiTapChonXe";
import DemoProps from "./DemoProps/DemoProps";
import BaiTapShoeShop from "./BaiTapShoeShop/BaiTapShoeShop";
import DemoMiniRedux from "./DemoMiniRedux/DemoMiniRedux";
import BaiTapShoeShopRedux from "./BaiTapShoeShopRedux/BaiTapShoeShopRedux";

function App() {
  return (
    <div className="App">
      {/* <DemoClass />
      <DemoFunction /> */}
      {/* <BaiTapLayoutBS /> */}
      {/* <DataBind /> */}
      {/* <DemoState /> */}
      {/* <BaiTapChonXe /> */}
      {/* <DemoProps /> */}
      {/* <BaiTapShoeShop /> */}
      {/* <DemoMiniRedux /> */}
      <BaiTapShoeShopRedux />
    </div>
  );
}

export default App;
