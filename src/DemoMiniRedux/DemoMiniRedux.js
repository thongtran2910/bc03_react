import React, { Component } from "react";
import { connect } from "react-redux";
import { tangSoLuongAction } from "./redux/action";

class DemoMiniRedux extends Component {
  render() {
    return (
      <div className="text-center display-4">
        <button
          onClick={this.props.handleTangNumber}
          className="btn btn-success mx-2"
        >
          Tăng
        </button>
        {this.props.giaTri}
        <button className="btn btn-warning mx-2">Giảm</button>
      </div>
    );
  }
}

let mapStateToProps = (state) => {
  return {
    giaTri: state.number.number,
  };
};

let mapDispatchToProps = (dispatch) => {
  return {
    handleTangNumber: () => {
      dispatch(tangSoLuongAction(100));
    },
  };
};
export default connect(mapStateToProps, mapDispatchToProps)(DemoMiniRedux);
