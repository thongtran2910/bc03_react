// action creator
import { TANG_SO_LUONG } from "./constants";

export const tangSoLuongAction = (soLuong) => {
  return {
    type: TANG_SO_LUONG,
    payload: soLuong,
  };
};
