import { TANG_SO_LUONG } from "./constants";

let initialState = {
  number: 0,
};

export const numberReducer = (state = initialState, { type, payload }) => {
  switch (type) {
    case TANG_SO_LUONG: {
      console.log("yes");
      state.number += payload;
      return { ...state };
    }
    default:
      return state;
  }
};
