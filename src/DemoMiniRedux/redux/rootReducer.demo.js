import { combineReducers } from "redux";
import { numberReducer } from "./NumberReducer";

export const rootReducerDemo = combineReducers({
  number: numberReducer,
});
