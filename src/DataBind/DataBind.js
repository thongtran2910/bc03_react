import React, { Component } from "react";

export default class DataBind extends Component {
  username = "Alice Nguyen";
  render() {
    let username = "Bob";
    let usernameClass = "text-danger";
    let renderIntroduce = () => {
      return <div>Tôi yêu Việt Nam</div>;
    };
    return (
      <div
        style={{ backgroundColor: "black", fontSize: "50px" }}
        className={usernameClass}
      >
        Hello: {username}
        {renderIntroduce()}
      </div>
    );
  }
}
