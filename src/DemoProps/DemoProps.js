import React, { Component } from "react";
import ChildComponent from "./Child.component";
import PropsChildren from "./PropsChildren";

export default class DemoProps extends Component {
  state = {
    username: "Alice",
  };
  handleChangeName = (name) => {
    this.setState({
      username: name,
    });
  };
  render() {
    return (
      <div>
        <ChildComponent
          helloTitle={this.state.username}
          handleOnclick={this.handleChangeName}
        />
        <h2>Demo PropChildren</h2>
        <PropsChildren>Tôi yêu Việt Nam</PropsChildren>
      </div>
    );
  }
}
