import React, { Component } from "react";

export default class Cart extends Component {
  render() {
    return (
      <div className="container">
        <table className="table">
          <thead>
            <td>ID</td>
            <td>Tên sản phẩm</td>
            <td>Giá sản phẩm</td>
            <td>Số lượng</td>
            <td>Thao tác</td>
          </thead>
          <tbody>
            {this.props.gioHang.map((item) => {
              return (
                <tr>
                  <td>{item.id}</td>
                  <td>{item.name}</td>
                  <td>{item.price}</td>
                  <td>{item.soLuong}</td>
                  <td>
                    <button
                      onClick={() => {
                        this.props.handleXoaSanPham(item.id);
                      }}
                      className="btn btn-danger"
                    >
                      Xoá
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      </div>
    );
  }
}
