import React, { Component } from "react";
import Header from "./Header";
import Banner from "./Banner";
import Item from "./Item";
import Footer from "./Footer";

export default class BaiTapLayoutBS extends Component {
  render() {
    return (
      <div>
        <Header />
        <div className="container">
          <Banner />
          <div className="row">
            <Item />
            <Item />
            <Item />
            <Item />
          </div>
        </div>
        <Footer />
      </div>
    );
  }
}
